"""
"""


from odoo import models, fields 

class Brand(models.Model):
    """
    """

    _name = "sale.module.brand"
    _description = "Allows you to create a brand to which a product belongs"

    name = fields.Char(string='Nombre', required=True)
    state = fields.Boolean(string='Estado', default=True)

    #relation departement whit brand
    department_id = fields.Many2one('sale.module.department', string='Departamento', required=True)
    product_ids = fields.One2many('product.template', 'brand_id', required=True, readonly=True)