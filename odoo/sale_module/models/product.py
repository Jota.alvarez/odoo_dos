"""
"""

from odoo import models, fields

class Product(models.Model):
    """
    """

    _name = "product.template"
    _inherit = "product.template"
    _description = "This module inherits from _modules and relates a product to a brand."

    description = fields.Text(string='Descripción')

    #relation of product whit brand
    brand_id = fields.Many2one('sale.module.brand', string='Marca',required=True)