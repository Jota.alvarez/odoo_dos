"""
This is a python module that create un departmen
"""

from odoo import models, fields

class Department(models.Model):
    """
    This .....
    """

    _name = "sale.module.department"
    _description = "Allows you to create a department, which receives: name, description (optional), discount and a status."
    
    name = fields.Char(string='Departamento', required=True)
    description = fields.Text(String='Descripción',)
    discount = fields.Float(string='Descuento', required=True)
    state = fields.Boolean(string='Estado', default=True)

    #relation
    brand_ids = fields.One2many('sale.module.brand', 'department_id', required=True, readonly=True)

